const path = require("path");
const fs = require("fs")
const { traverseDir, formatDate } = require("./common/utils");
const ParseVue = require("./core/parseVue");
const Logger = require("./core/logger");
const BaseParse = require("./core/baseParse");

exports.VerifyProject = class VerifyProject extends Logger {

    /**
     * 项目名称
     * @type {String}
     */
    projectName

    /**
     * 项目名称的根目录
     * @type {String}
     */
    rootDir

    /**
     * 文档输出的绝对路径
     * @type {String}
     */
    outputDir

    /**
     * 自定义规则的key后缀
     * @type {String}
     */
    ruleSuffix = "Lm"

    /**
     * 当前处理的手柄
     * @type {BaseParse}
     */
    currentHandler;

    /**
     * 当前处理的文件
     * @type {String}
     */
    currentFile

    /**
     * 当前处理的文件类型
     * @type {String}
     */
    currentType

    /**
     * 根目录下的所有文件信息
     * @type {Array}
     */
    allFiles = []

    /**
     * 根目录下的所有目录信息
     * @type {Array}
     */
    allDirs = []

    /**
     * 根目录下文件的总数
     * @type {Number}
     */
    fileTotal = 0

    /**
     * 根目录下目录的总数
     * @type {Number}
     */
    dirTotal = 0

    referenceRelation = [];

    constructor(config) {
        super();
        this.projectName = config.projectName;
        this.outputDir = path.join(__dirname, "output")
        this.rootDir = config.rootDir.replace(/\\\\/g, "/")
        this.dirRules = []
        this.fileRules = [];
        this.dirRuleResult = { info: [], warn: [], error: [] }
        this.fileRuleResult = { info: [], warn: [], error: [] }
        config.rules.forEach(el => {
            if (el.classify === "directory") {
                this.dirRules.push(Object.assign({ suffix: [], exclude: [] }, el))
            } else if (el.classify === "file") {
                this.fileRules.push(Object.assign({ suffix: [], exclude: [] }, el))
            }
            this[el.ruleKey + this.ruleSuffix] = { info: [], warn: [], error: [] };
        })
    }

    /**
     * 开始验证
     */
    begin() {
        this.beginTime = Date.now();
        this.title(`开始验证项目<<${this.projectName}>>`);
        this.info(`[${formatDate(new Date(), "hh:mm:ss")}] 开始遍历所有的文件`)
        const res = traverseDir(this.rootDir);
        this.allFiles = res.files;
        this.fileTotal = this.allFiles.length;
        this.allDirs = res.dirs;
        this.dirTotal = this.allDirs.length;
        this.info(`[${formatDate(new Date(), "hh:mm:ss")}] 完成遍历所有的文件`)
        this.verifyDirectorys();
        this.normalizeDirRule();
        this.verifyFiles()
        this.endTime = Date.now();
    }

    verifyFiles() {
        this.info(`[${formatDate(new Date(), "hh:mm:ss")}] 开始验证文件类规则`)
        this.allFiles.forEach((fPath, index) => {
            this.currentFile = fPath;
            this.currentType = path.extname(fPath).slice(1);
            this.info(`[${formatDate(new Date(), "hh:mm:ss")}] [${index + 1}/${this.fileTotal}] 开始处理文件${this.currentFile}`)
            this.parseFile()
        })
        this.info(`[${formatDate(new Date(), "hh:mm:ss")}] 完成验证文件类规则`)
    }

    parseFile() {
        if (this.currentType === "vue") {
            this.currentHandler = new ParseVue(this.currentFile, this.rootDir, this.fileRules);
        } else {
            this.currentHandler = new BaseParse(this.currentFile, this.rootDir, this.fileRules);
        }
        this.currentHandler.normalizeRule();
        this.currentHandler.end();
        this.currentHandler.writeFile(this.outputDir)
        this.normalizeFileRule();
        this.addReferenceRelation(this.currentHandler)
        this.currentHandler = null;
        this.info(`[已处理] 文件类型[${this.currentType}] 文件[${this.currentFile}]`)
    }

    verifyDirectorys() {
        this.info(`[${formatDate(new Date(), "hh:mm:ss")}] 开始验证目录类规则`)
        this.dirRules.forEach((el) => {
            this.info(`[${formatDate(new Date(), "hh:mm:ss")}] 开始验证规则 ${el.ruleName}`)
            this.verifyDirectory(el)
            this.writeRuleFile(el)
        })
        this.info(`[${formatDate(new Date(), "hh:mm:ss")}] 完成验证目录类规则`)
    }

    verifyDirectory(rule) {
        this.allDirs.forEach(dirPath => {
            if (rule.ruleKey === "dirName") {
                if (rule.exclude.filter(el => dirPath.indexOf(el) > -1).length === 0) {
                    let reg = /^[a-z][a-z0-9]+([A-Z][a-z0-9]+)*$/;
                    dirPath.split("/").forEach(dir => {
                        if (!reg.test(dir)) {
                            let msg = ` 目录名:${dir} 目录:${dirPath}`
                            this.appendRuleMessage(rule, msg)
                        }
                    })
                }
            }
        })
    }

    /**
     * 获取验证项目的耗时
     */
    getElapsedTime() {
        return Math.round(this.endTime - this.beginTime)
    }

    /**
     * 归一化目录类规则
     */
    normalizeDirRule() {
        this.dirRules.map(el => el.ruleKey).map(el => this.getSpecificRules(el)).forEach(el => {
            for (let key in this.dirRuleResult) {
                this.dirRuleResult[key] = this.dirRuleResult[key].concat(el[key])
            }
        })
    }

    /**
     * 归一化目录类规则
     */
    normalizeFileRule() {
        this.fileRules.map(el => el.ruleKey).map(el => this.currentHandler.getSpecificRules(el)).forEach(el => {
            for (let key in this.fileRuleResult) {
                this.fileRuleResult[key] = this.fileRuleResult[key].concat(el[key])
            }
        })
    }

    /**
     * 获取特定的文件类校验规则
     * @param {String} ruleKey 
     * @returns {Object}
     */
    getSpecificRules(ruleKey) {
        return this[ruleKey + this.ruleSuffix]
    }

    appendRuleMessage(rule, message) {
        let msg = rule.message.replace("{message}", message);
        if (!this[rule.ruleKey + this.ruleSuffix][rule.type].includes(msg)) {
            this[rule.ruleKey + this.ruleSuffix][rule.type].push(msg)
        }
    }

    writeRuleFile(rule) {
        let rules = this[rule.ruleKey + this.ruleSuffix];
        this.writeFile(rule.outputFileName, rules)
    }

    writeFile(fileName, data) {
        let file = `${this.outputDir}/${fileName}.json`;
        fs.writeFileSync(file, JSON.stringify(data, null, 4))
    }

    /**
     * 输出验证结果
     */
    getResult() {
        this.writeReferenceRelationFile();
        let content = fs.readFileSync("./output/output.temp.md").toString();
        let identifyReg = /\{([a-zA-Z]+)\}/g;
        let resultData = this.getCheckResult();
        let newContent = content.replace(identifyReg, ($0, $1) => {
            if ($1) {
                return resultData[$1] ?? "未知"
            }
        })
        fs.writeFileSync("./output/验证报告.md", newContent)
    }

    getCheckResult() {
        const data = {
            projectName: this.projectName,
            buildTime: formatDate(new Date),
            elapsedTime: this.getElapsedTime(),
            inputDir: this.rootDir,
            outputDir: this.outputDir,
            total: this.fileTotal,
            infoFileTotal: this.fileRuleResult.info.length,
            warnFileTotal: this.fileRuleResult.warn.length,
            errorFileTotal: this.fileRuleResult.error.length,
            infoDirTotal: this.dirRuleResult.info.length,
            warnDirTotal: this.dirRuleResult.warn.length,
            errorDirTotal: this.dirRuleResult.error.length,
            fileMessageTotal: this.fileRuleResult.info.length + this.fileRuleResult.warn.length + this.fileRuleResult.error.length,
            dirMessageTotal: this.dirRuleResult.info.length + this.dirRuleResult.warn.length + this.dirRuleResult.error.length,
        }
        data.infoFileRatio = this.calcRatio(data.infoFileTotal, data.fileMessageTotal)
        data.warnFileRatio = this.calcRatio(data.warnFileTotal, data.fileMessageTotal)
        data.errorFileRatio = this.calcRatio(data.errorFileTotal, data.fileMessageTotal)
        data.infoDirRatio = this.calcRatio(data.infoDirTotal, data.dirMessageTotal)
        data.warnDirRatio = this.calcRatio(data.warnDirTotal, data.dirMessageTotal)
        data.errorDirRatio = this.calcRatio(data.errorDirTotal, data.dirMessageTotal)
        Object.assign(data, this.getRules())
        return data;
    }

    calcRatio(size, totol) {
        return (+(size / totol) * 100).toFixed(2) + "%"
    }

    getRules() {
        let dirRules = '', fileRules = "";
        this.dirRules.forEach(el => {
            dirRules += `+ 规则名称 ${el.ruleName} 提示类型 ${el.type}\r\n`
        })
        this.fileRules.forEach(el => {
            fileRules += `+ 规则名称 ${el.ruleName} 提示类型 ${el.type}\r\n`
        })
        return { dirRules, fileRules }
    }

    addReferenceRelation(data) {
        this.referenceRelation.push({
            id: data.id,
            fileName: data.fileName,
            shortPath: data.shortPath,
            outputFile: data.fileName + "_" + this.id + "_rule.json",
            ElapsedTime: data.getElapsedTime()
        })
    }

    writeReferenceRelationFile() {
        let message = `
## 文件引用对应关系图

**文件日志不一定存在 当存在校验不通过的时候才有日志输出**

| 文件标识| 文件名称 | 耗时(ms)|文件短路径 |文件日志 |
| - | - | - | - |- |`
        this.referenceRelation.forEach(el => {
            message += `\r\n| ${el.id} | ${el.fileName} |${el.ElapsedTime} |${el.shortPath} | ${el.outputFile} |`
        })
        let file = `${this.outputDir}/文件引用对应关系图.md`;
        fs.writeFileSync(file, message)
    }
}