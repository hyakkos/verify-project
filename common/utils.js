const path = require("path")
const fs = require("fs")
exports.clearDir = function clearDir(rootPath) {
    function rmDirOrFile(dir) {
        let files = fs.readdirSync(dir);
        if (files.length) {
            files.forEach(function (item) {
                let fPath = path.join(dir, item);
                let stat = fs.statSync(fPath);
                if (stat.isDirectory() === true) {
                    rmDirOrFile(fPath);
                }
                if (stat.isFile() === true) {
                    fs.rmSync(fPath)
                }
            });
        } else {
            if (dir !== rootPath) {
                fs.rmSync(dir)
            }
        }
    }
    rmDirOrFile(rootPath)
}

/**
 * 遍历指定的目录
 * @param {string} rootPath - 需要遍历的目录
 * @returns {<string>} - 遍历后的文件名集合
 */
exports.traverseDir = function (rootPath) {
    let result = {
        files: [],
        dirs: []
    }
    function findFile(dir) {
        let files = fs.readdirSync(dir);
        files.forEach(function (item) {
            let fPath = path.join(dir, item);
            let stat = fs.statSync(fPath);
            if (stat.isDirectory() === true) {
                result.dirs.push(fPath.slice(rootPath.length + 1).replace(/\\/g, "/"));
                findFile(fPath);
            }
            if (stat.isFile() === true) {
                result.files.push(fPath.slice(rootPath.length + 1).replace(/\\/g, "/"));
            }
        });
    }
    findFile(rootPath);
    return result;
}

exports.formatDate = function (date, fmt = "yyyy-MM-dd hh:mm:ss") {
    var o = {
        "M+": date.getMonth() + 1,
        "d+": date.getDate(),
        "h+": date.getHours(),
        "m+": date.getMinutes(),
        "s+": date.getSeconds(),
        "S": date.getMilliseconds()
    };
    if (/(y+)/.test(fmt))
        fmt = fmt.replace(RegExp.$1, (date.getFullYear() + "").substring(4 - RegExp.$1.length));
    for (var k in o)
        if (new RegExp("(" + k + ")").test(fmt))
            fmt = fmt.replace(RegExp.$1, (RegExp.$1.length == 1) ? (o[k]) : (("00" + o[k]).substring(("" + o[k]).length)));
    return fmt;
}  