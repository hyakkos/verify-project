const BaseParse = require("./baseParse");
const { DocFragment } = require("@jsextends/parsehtml")

module.exports = class ParseVue extends BaseParse {
    scriptNodes = []

    templateNode = ""

    styleNodes = []

    constructor(fpath, rootDir, rules) {
        super(fpath, rootDir, rules);
        this.parseTemplate();
        this.parseScript()
        this.parseStyle();
        this.applyVueRule();
        this.end();
    }

    applyVueRule() {
        this.rules.forEach(el => {
            if (el.suffix.includes(this.type.toLowerCase())) {

            }
        })
    }

    /**
     * 解析vue文件中的template
     */
    parseTemplate() {
        let tempReg = /<template/i
        if (tempReg.test(this.content)) {
            let startIndex = this.content.indexOf('<template');
            let endIndex = this.content.lastIndexOf('</template>') + '</template>'.length;
            this.templateNode = new DocFragment(this.content.slice(startIndex, endIndex));
        }
    }

    /**
     * 解析vue文件中的script
     */
    parseScript() {
        let scriptReg = /<script/i
        if (scriptReg.test(this.content)) {
            let startIndex = this.content.indexOf(`<script`);
            let endIndex = this.content.indexOf(`</script>`) + + `</script>`.length;
            this.scriptNodes = [new DocFragment(this.content.slice(startIndex, endIndex))];
            startIndex = this.content.indexOf(`<script`, endIndex);
            while (startIndex != -1) {
                endIndex = this.content.indexOf(`</script>`, startIndex) + `</script>`.length;
                this.scriptNodes.push(new DocFragment(this.content.slice(startIndex, endIndex)));
                startIndex = this.content.indexOf(`<script`, endIndex);
            }
        }
    }

    /**
     * 解析vue文件中的style
     */
    parseStyle() {
        let styleReg = /<style/i
        if (styleReg.test(this.content)) {
            let startIndex = this.content.indexOf(`<style`);
            let endIndex = this.content.indexOf(`</style>`) + `</style>`.length;;
            this.styleNodes.push(new DocFragment(this.content.slice(startIndex, endIndex)))
            startIndex = this.content.indexOf(`<style`, endIndex);
            while (startIndex != -1) {
                endIndex = this.content.indexOf(`</style>`, endIndex) + `</style>`.length;;
                this.styleNodes.push(new DocFragment(this.content.slice(startIndex, endIndex)))
                startIndex = this.content.indexOf(`<style`, endIndex);
            }
        }
    }
}