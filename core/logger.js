module.exports = class Logger {
    info(message) {
        console.log(`\x1B[32m${message}\x1B[0m`)
    }

    warn(message) {
        console.log(`\x1B[33m${message}\x1B[0m`)
    }

    error(message) {
        console.log(`\x1B[31m${message}\x1B[0m`)
    }

    title(message) {
        console.log(`\x1B[34m${message}\x1B[0m`)
    }

}