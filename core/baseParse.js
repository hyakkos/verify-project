const fs = require("fs");
const path = require("path");
module.exports = class BaseParse {
    /**
     * 唯一标识
     * @type {String}
     */
    id

    /**
     * 文件名无后缀
     * @type {String}
     */
    fileName

    /**
     * 开始处理文件的时间
     * @type {Number}
     */
    beginTime = 0;

    /**
     * 完成文件处理的时间
     * @type {Number}
     */
    endTime = 0;

    /**
     * 文件的绝对路径
     * @type {String}
     */
    fPath

    /**
     * 文件的短路径
     * @type {String}
     */
    shortPath

    /**
    * 文件类型
    * @type {String}
    */
    type

    /**
     * 文件所在目录
     * @type {String}
     */
    dir

    /**
     * 文件的内容
     * @type {String}
     */
    content

    /**
     * 自定义规则的key后缀
     * @type {String}
     */
    ruleSuffix = "Lm"

    /**
     * 规则校验输出的文件信息集合
     * @type {Array}
     */
    outputFile = [];

    rules = [];

    verifyResult = {
        info: [],
        warn: [],
        error: []
    }

    /**
     * 构造函数
     * @param {String} fPath -文件的相对于根目录的相对路径
     * @param {String} rootDir -文件的根目录
     * @param {Array} rules - 校验规则
     */
    constructor(fPath, rootDir, rules) {
        this.beginTime = Date.now();
        this.id = "xxxxxxxxxxx".replace(/x/g, () => {
            return (Math.random() * 16 | 0).toString(16).toUpperCase()
        })
        this.fullPath = path.join(rootDir, fPath).replace(/\\/g, "/")
        this.type = path.extname(this.fullPath).slice(1)
        this.fileName = path.basename(this.fullPath, "." + this.type)
        this.fPath = path.join(rootDir, fPath);
        this.dir = this.fullPath.slice(0, this.fullPath.indexOf(this.fileName) - 1)
        this.parseShortDir(rootDir);
        this.content = fs.readFileSync(this.fPath).toString().trim();
        rules.forEach(el => {
            this.rules.push(Object.assign({ suffix: [], exclude: [] }, el))
            this[el.ruleKey + this.ruleSuffix] = { info: [], warn: [], error: [] };
        })
        this.applyRule();
    }
    applyRule() {
        this.rules.forEach(el => {
            if (!el.suffix.length) {
                if (el.ruleKey === "fileName") {
                    let reg = /^[a-z][a-z0-9]+([A-Z][a-z0-9]+)*$/;
                    if (!reg.test(this.fileName) && this.fileName !== "App") {
                        let msg = `文件短目录:${this.shortPath} 文件名:${this.fileName}`
                        this.appendRuleMessage(el, msg)
                    }
                }
            }
        })
    }

    end(){
       this.endTime = Date.now(); 
    }

    /**
     * 获取特定的文件类校验规则
     * @param {String} ruleKey 
     * @returns {Object}
     */
    getSpecificRules(ruleKey) {
        return this[ruleKey + this.ruleSuffix]
    }

    /**
     * 获取无差别文件类校验规则 即el.suffix为空
     * @returns {Array}
     */
    getNoSuffixRules() {
        return this.rules.filter(el => !el.suffix.length).map(el => el.ruleKey).map(el => this.getSpecificRules(el));
    }

    /**
     * 获取所有的文件类校验规则
     * @returns {Array}
     */
    getAllRule() {
        return this.rules.map(el => el.ruleKey).map(el => this.getSpecificRules(el))
    }

    /**
     * 解析文件的短目录
     * @param {String} rootDir -文件的根目录 
     */
    parseShortDir(rootDir) {
        this.shortPath = this.fullPath.slice(rootDir.length);
        if (this.shortPath[0] === "/") {
            this.shortPath = this.shortPath.slice(1);
        }
    }

    /**
     * 获取处理文件的耗时
     */
    getElapsedTime() {
        return Math.round(this.endTime - this.beginTime)
    }

    appendRuleMessage(rule, message) {
        let msg = rule.message.replace("{message}", message);
        if (!this[rule.ruleKey + this.ruleSuffix][rule.type].includes(msg)) {
            this[rule.ruleKey + this.ruleSuffix][rule.type].push(msg)
        }
    }

    hasMessage() {
        for (let key in this.verifyResult) {
            if (this.verifyResult[key].length) {
                return true;
            }
        }
        return false
    }

    normalizeRule() {
        this.getAllRule().forEach(el => {
            for (let key in this.verifyResult) {
                this.verifyResult[key] = this.verifyResult[key].concat(el[key])
            }
        })
    }

    writeFile(outputDir) {
        if (this.hasMessage()) {
            let file = `${outputDir}/${this.fileName}_${this.id}_rule.json`;
            fs.writeFileSync(file, JSON.stringify(this.verifyResult, null, 4))
        }
    }
}