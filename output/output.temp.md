## 关于《{projectName}》验证报告
### 报告输出时间：{buildTime}
### 项目验证耗时 {elapsedTime}ms
+ 项目输入路径: {inputDir}
+ 项目输出路径: {outputDir}

## 校验规则如下

### 目录类规则

{dirRules}

### 文件类规则
{fileRules}

## 统计各类提示信息如下

+ 文件类信息总数 {fileMessageTotal}
+ 目录类信息总数 {dirMessageTotal}

| 类型 | 个数 | 占比 |
| - | - | - |
| 文件类提示信息 | {infoFileTotal} | {infoFileRatio} |
| 文件类警告信息 | {warnFileTotal} | {warnFileRatio} |
| 文件类错误信息 | {errorFileTotal} | {errorFileRatio} |
| 目录类提示信息 | {infoDirTotal} | {infoDirRatio} |
| 目录类警告信息 | {warnDirTotal} | {warnDirRatio} |
| 目录类错误信息 | {errorDirTotal} | {errorDirRatio} |


[文件引用对应关系图](./文件引用对应关系图.md)

