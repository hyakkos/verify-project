**verifyProject**

| 关键字 | 描述 |
| - | - | 
| 作者 | usernameisregistered |
| 时间 | 2022-05-08 11:00|
| 描述 | 一款nodejs验证项目是否合规的工具 |

## 配置信息

```
{
    "projectName": "项目名称",
    "rootDir": "项目根目录",
    "rules": [
        {
            "ruleName": "规则名称",
            "suffix": ["[可选]当前规则适用的文件后缀，不获取真实的文件类型，使用文件后缀判断 当规则为file有效"],
            "exclude": ["[可选]需要排除的文件名称或者文件路径 相对于rootDir的绝对路径"],
            "type": "[info|warn|error]当前规则的提示类型",
            "message": "当前规则的提示语句 变量名为message",
            "classify": "[file|directory]当前规则的提示类型",
            "outputFileName": "输出文件名 当规则为directory有效",
            "ruleKey": "规则的key必须是小驼峰[a-z]+([A-Z][a-z]+)?"
        }
    ]
}
```

## 校验规则

### 小驼峰命名法

> 第一个单词首字母小写，剩余单词首字母都大写
> 允许字符a-z0-9A-Z

### 目录类规则
```
{
    "ruleName": "目录名使用小驼峰命名法",
    "outputFileName": "目录名_小驼峰命名法",
    "ruleKey": "dirName",
    "type": "error",
    "message": "[{message}]使用小驼峰命名法",
    "classify": "directory",
    "exclude": ["i18n", "components/H5Player"],
}
```

### 文件类规则

```
{
    "ruleName": "文件名使用小驼峰命名法",
    "outputFileName": "文件名_小驼峰命名法",
    "ruleKey": "fileName",
    "type": "error",
    "message": "[{message}]使用小驼峰命名法",
    "classify": "file",
    "exclude": [
        "i18n"
    ]
}
```